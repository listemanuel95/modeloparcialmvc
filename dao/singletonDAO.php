<?php

namespace dao;

/**
 * En Metodología de Sistemas nos hacen usar patrón Singleton para los DAOs (para no instanciarlos constantemente).
 * Si bien no se pide en Laboratorio IV, lo agrego igual.
 */
abstract class SingletonDAO
{
    private static $instances = array();
    protected function __construct() {}
    protected function __clone() {}

    public static function get_instance()
    {
        $cls = get_called_class();
        if (!isset(self::$instances[$cls]))
            self::$instances[$cls] = new static;
            
        return self::$instances[$cls];
    }
}

?>