<?php

namespace dao;

use model\Category as Category;
use interfaces\DAOCrud as DAOCrud;

class CategoryDAO extends SingletonDAO implements DAOCrud 
{

    public function create($instance)
    {
        $conn = new Connection();
        $conn = $conn->get_connection();

        if($instance instanceof Category)
        {
            try {

                $statement = $conn->prepare("INSERT INTO `categorias` (`nombre`, `id_superior`) VALUES (:c_name, :c_parent)");
    
                $statement->bindValue(':c_name', $instance->get_name());
                $statement->bindValue(':c_parent', $instance->get_parent_id());

                $statement->execute();
            } catch (\Exception $e) {
                echo 'EXCEPCION: ' . $e->getMessage();
            }
        } else {
            throw new \Exception('ERROR EN CATEGORY->CREATE');
        }
        
    }

    public function retrieve($instance)
    {
        
    }

    public function update($instance)
    {

    }

    public function delete($instance)
    {

    }

    /**
     * Devuelve una categoría según el ID
     */
    public function retrieve_by_id($id)
    {
        $conn = new Connection();
        $conn = $conn->get_connection();

        try {
            $statement = $conn->prepare("SELECT * FROM `categorias` WHERE `id` = $id");
            $statement->execute();

            $res = $statement->fetch();
            if($res['id'] != null)
                return new Category($res['nombre'], $res['id_superior'], $res['id']);

            return null;
        } catch (\Exception $e) {
            echo 'EXCEPCION: ' . $e->getMessage();
        }
    }

    /**
     * Devuelve todas las categorías, para armar el <select>
     */
    public function retrieve_all()
    {
        $conn = new Connection();
        $conn = $conn->get_connection();

        try {
            $statement = $conn->prepare("SELECT * FROM `categorias`");
            $statement->execute();

            $res = $statement->fetchAll();
            $ret = array();

            foreach($res as $cat)
                $ret[] = new Category($cat['nombre'], $cat['id_superior'], $cat['id']);

            return $ret;
        } catch (\Exception $e) {
            echo 'EXCEPCION: ' . $e->getMessage();
        }
    }
} 

?>