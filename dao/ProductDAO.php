<?php

namespace dao;

use model\Product as Product;
use model\Category as Category;
use interfaces\DAOCrud as DAOCrud;

class ProductDAO extends SingletonDAO implements DAOCrud 
{

    /**
     * Insertamos el producto en la base de datos
     */
    public function create($instance)
    {
        $conn = new Connection();
        $conn = $conn->get_connection();

        if($instance instanceof Product)
        {
            try {
                $statement = $conn->prepare("INSERT INTO `productos` (`id_categoria`, `nombre`, `stock`, `precio`, `imagen`)
                                             VALUES (:c_id, :p_name, :p_stock, :p_price, :p_img)");

                $statement->bindValue(':c_id', $instance->get_category()->get_id());
                $statement->bindValue(':p_name', $instance->get_name());
                $statement->bindValue(':p_stock', $instance->get_stock());
                $statement->bindValue(':p_price', $instance->get_price());
                $statement->bindValue(':p_img', $instance->get_img_path());
                                            
                $statement->execute();

            } catch (\Exception $e) {
                echo 'EXCEPCION: ' . $e->getMessage();
            }
        } else {
            throw new \Exception('ERROR EN PRODUCT->CREATE');
        }
    }

    public function retrieve($instance)
    {

    }

    public function update($instance)
    {

    }

    public function delete($instance)
    {

    }

    /**
     * Devuelve todos los categorías, para armar la tabla
     */
    public function retrieve_all()
    {
        $conn = new Connection();
        $conn = $conn->get_connection();

        try {
            $statement = $conn->prepare("SELECT `P`.`id` AS `p_id`, `P`.`nombre` AS `p_name`, `P`.`stock` AS `p_stock`,
                                                `P`.`precio` AS `p_price`, `P`.`imagen` AS `p_img`, `C`.`id` AS `c_id`,
                                                `C`.`nombre` AS `c_name`, `C`.`id_superior` AS `c_sup` 
                                                FROM `productos` AS `P` JOIN `categorias` AS `C` ON `P`.`id_categoria` = `C`.`id`");
            $statement->execute();

            $res = $statement->fetchAll();
            $ret = array();

            foreach($res as $prod)
                $ret[] = new Product($prod['p_name'], $prod['p_stock'], $prod['p_price'], $prod['p_img'], new Category($prod['c_name'], $prod['c_sup'], $prod['c_id']), $prod['p_id']);

            return $ret;
        } catch (\Exception $e) {
            echo 'EXCEPCION: ' . $e->getMessage();
        }
    }
} 

?>