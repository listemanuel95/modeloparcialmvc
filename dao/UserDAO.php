<?php

namespace dao;

use model\User as User;
use interfaces\DAOCrud as DAOCrud;

class UserDAO extends SingletonDAO implements DAOCrud 
{

    public function create($instance)
    {
        
    }

    /**
     * Buscamos el usuario recibido por parámetro en la base de datos (para el login)
     */
    public function retrieve($instance)
    {
        // Connection está en este mismo namespace, puedo usarla tranquilamente
        $conn = new Connection();
        $conn = $conn->get_connection();

        if($instance instanceof User)
        {
            try {
                $statement = $conn->prepare("SELECT * FROM `administradores` WHERE `nombre` = :c_name AND `pass` = :c_pass");
    
                $statement->bindValue(':c_name', $instance->get_name());
                $statement->bindValue(':c_pass', md5($instance->get_pass()));
    
                $statement->execute();
    
                $res = $statement->fetch();
    
                if($res['id'] != null)
                    return new User($res['nombre'], $res['pass'], $res['id']);
    
                return null;
            } catch (\Exception $e) {
                echo 'EXCEPCION: ' . $e->getMessage();
            }
        } else {
            throw new \Exception('ERROR EN USER->RETRIEVE');
        }
        
    }

    public function update($instance)
    {

    }

    public function delete($instance)
    {

    }
} 

?>