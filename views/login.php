<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- BOOTSTRAP CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    
    <title>Repaso Parcial</title>
</head>
<body>

    <div class="container">
        <br><br><br>
        <div class="row">
            <div class="col-12 text-center">
                <h1>Modelo de Parcial - Login</h1>
            </div>
        </div>
        <br><hr><br>
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6 text-center">
                <form action="login" method="POST">
                    <input type="text" class="form-control" placeholder="Nombre de usuario..." name="username" required>
                    <br><input type="password" class="form-control" placeholder="Contraseña..." name="pass" required>
                    <br><button type="input" class="btn btn-primary">Ingresar</button>
                </form>
            </div>
            <div class="col-3"></div>
        </div>

        <!-- POR SI HUBO ERRORES DE LOGUEO -->
        <?php if(isset($_GET['error'])) { ?>
            <br><br>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Error</strong>, datos inválidos.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php } ?>
    </div>
    
    <!-- jQUERY JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <!-- BOOTSTRAP JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>