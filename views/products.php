<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- BOOTSTRAP CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    
    <title>Repaso Parcial</title>
</head>
<body>

    <div class="container">
        <br><br><br>
        <div class="row">
            <div class="col-12 text-center">
                <h1>Modelo de Parcial - Productos</h1>
                <h5><a href="categories">Ver categorías</a></h5>
            </div>
        </div>
        <br><hr><br>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8 text-center">
                <h3>Productos Existentes</h3>
                <table class="table table-striped">
                    <thead>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Stock</th>
                        <th>Precio</th>
                        <th>Categoría</th>
                        <th>Imagen</th>
                    </thead>
                    <tbody>
                        <?php foreach($productsDB as $prod) { ?>
                            <tr>
                                <td><?php echo $prod->get_id(); ?></td>
                                <td><?php echo $prod->get_name(); ?></td>
                                <td><?php echo $prod->get_stock(); ?></td>
                                <td><?php echo $prod->get_price(); ?></td>
                                <td><?php echo $prod->get_category()->get_name(); ?></td>
                                <td><img width="64" height="64" src="<?php echo $prod->get_img_path(); ?>"></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-2"></div>
        </div>
        <br><hr><br>
        <div class="row">
            <div class="col-4"></div>
            <div class="col-4">
                <h3 class="text-center">Alta de Producto</h3><br>
                <form action="products/create_product" method="POST" enctype="multipart/form-data">
                    Categoría:<br>
                    <br><select name="catparent" class="form-control">
                        <?php foreach($categoriesDB as $cat) { ?>
                            <option value="<?php echo $cat->get_id(); ?>"><?php echo $cat->get_name(); ?></option>
                        <?php } ?>
                    </select>
                    <br>Nombre:<br>
                    <br><input type="text" class="form-control" name="name" required>
                    <br>Stock:<br>
                    <br><input type="number" value="10" class="form-control" name="stock" required>
                    <br>Precio:<br>
                    <br><input type="number" value="20" class="form-control" name="price" required>
                    <br>Imagen:<br>
                    <br><input type="file" class="form-control" name="file" required>
                    <br><button type="submit" class="btn btn-primary form-control">Crear Producto</button>
                    <br><br>
                </form>
            </div>
            <div class="col-4"></div>
        </div>

        <!-- POR SI SE CREO EXITOSAMENTE EL PRODUCTO -->
        <?php if(isset($_GET['success'])) { ?>
            <br><br>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Producto agregado</strong> exitosamente.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php } ?>

        <!-- POR SI HUBO ERRORES AL CARGAR LA IMAGEN -->
        <?php if(isset($_GET['error'])) { ?>
            <br><br>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Error</strong> al cargar la imagen. Cheque extensión del archivo, tamaño menor a 5MB y que no esté duplicado.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php } ?>
    </div>
    
    <!-- jQUERY JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <!-- BOOTSTRAP JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>