<?php

namespace interfaces;

Interface DAOCrud 
{
    public function create($instance);
    public function retrieve($instance);
    public function update($instance);
    public function delete($instance);
}

?>