<?php

namespace model;

class Product {

    private $id;
    private $name;
    private $stock;
    private $price;
    private $img_path;
    private $category;

    /**
     * En los constructores de clases que dependen de la base de datos, la ID siempre es opcional (puede tener una ID asignada o no)
     */
    public function __construct($name, $stock, $price, $img_path, $category, $id = null)
    {
        $this->name = $name;
        $this->stock = $stock;
        $this->price = $price;
        $this->img_path = $img_path;
        $this->category = $category;
        $this->id = $id;
    }

    public function get_name()
    {
        return $this->name;
    }

    public function get_stock()
    {
        return $this->stock;
    }

    public function get_price()
    {
        return $this->price;
    }

    public function get_img_path()
    {
        return $this->img_path;
    }

    public function get_category()
    {
        return $this->category;
    }

    public function get_id()
    {
        return $this->id;
    }
}

?>