<?php

namespace model;

class User {

    private $id;
    private $name;
    private $pass;

    /**
     * En los constructores de clases que dependen de la base de datos, la ID siempre es opcional (puede tener una ID asignada o no)
     */
    public function __construct($name, $pass, $id = null)
    {
        $this->name = $name;
        $this->pass = $pass;
        $this->id = $id;
    }

    public function get_name()
    {
        return $this->name;
    }

    public function get_pass()
    {
        return $this->pass;
    }

    public function get_id()
    {
        return $this->id;
    }
}

?>