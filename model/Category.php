<?php

namespace model;

class Category {

    private $id;
    private $name;
    private $parent_id;

    /**
     * En los constructores de clases que dependen de la base de datos, la ID siempre es opcional (puede tener una ID asignada o no)
     */
    public function __construct($name, $parent_id, $id = null)
    {
        $this->name = $name;
        $this->parent_id = $parent_id;
        $this->id = $id;
    }

    public function get_name()
    {
        return $this->name;
    }

    public function get_parent_id()
    {
        return $this->parent_id;
    }

    public function get_id()
    {
        return $this->id;
    }
}

?>