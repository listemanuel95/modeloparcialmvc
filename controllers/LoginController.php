<?php

namespace controllers;

use model\User as User;
use dao\UserDAO as UserDAO;

/**
 * La controladora de login chequea que los datos ingresados sean correctos y dispara la vista corresspondiente
 */
class LoginController {

    private $userdao;

    public function __construct()
    {
        $this->userdao = UserDAO::get_instance();
    }

    /**
     * Valida el logueo de un usuario (llega por POST)
     */
    public function index($user = null, $pass = null)
    {
        if($user == null || $pass == null)
        {
            header("Location: index");
        } else {
            $usuario = $this->userdao->retrieve(new User($user, $pass));

            if($usuario != null)
                header("Location: products");
            else
                header("Location: index?error=1");
        }
    }
}

?>