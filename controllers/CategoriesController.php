<?php

namespace controllers;

use model\Category as Category;
use dao\CategoryDAO as CategoryDAO;

/**
 * Controladora que dispara la vista de categorías y maneja su lógica
 */
class CategoriesController {

    private $categoriesdao;

    public function __construct()
    {
        $this->categoriesdao = CategoryDAO::get_instance();
    }

    /**
     * Todas las controladoras tienen método index()
     */
    public function index()
    {
        $categoriesDB = $this->categoriesdao->retrieve_all();
        require_once(ROOT . 'views/categories.php');
    }

    /**
     * Crea una nueva categoría y la inserta en la base de datos
     */
    public function create_category($parent = null, $name = null)
    {
        if($name == null)
        {
            header("Location: ../categories");
        } else {

            if($parent == 'null')
                $parent = null;

            $this->categoriesdao->create(new Category($name, $parent));
            header("Location: ../categories?success=1");
        }
    }

}

?>