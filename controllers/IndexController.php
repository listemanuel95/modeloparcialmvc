<?php

namespace controllers;

/**
 * En este caso, la controladora index dispara la vista de login, que es la página de inicio
 */
class IndexController {

    public function __construct()
    {

    }

    /**
     * Todas las controladoras tienen método index()
     */
    public function index()
    {
        require_once(ROOT . 'views/login.php');
    }

}

?>