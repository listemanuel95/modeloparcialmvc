<?php

namespace controllers;

use model\Category as Category;
use dao\CategoryDAO as CategoryDAO;

use model\Product as Product;
use dao\ProductDAO as ProductDAO;

/**
 * Controladora que dispara la vista de productos y maneja su lógica
 */
class ProductsController {

    private $categoriesdao;
    private $productsdao;

    public function __construct()
    {
        $this->categoriesdao = CategoryDAO::get_instance();
        $this->productsdao = ProductDAO::get_instance();
    }

    /**
     * Todas las controladoras tienen un index()
     */
    public function index()
    {
        $productsDB = $this->productsdao->retrieve_all();
        $categoriesDB = $this->categoriesdao->retrieve_all();
        require_once(ROOT . 'views/products.php');
    }

    /**
     * Inserta un producto en la base de datos
     */
    public function create_product($cat = null, $name = null, $stock = null, $price = null)
    {
        if($cat == null || $name == null || $stock == null || $price == null)
        {
            header("Location: ../products");
        } else {
            
            // validamos la imagen
            $img_path = $this->validate_img();

            if($img_path == null)
            {
                header("Location: ../products?error=1");
            } else {
                $category = $this->categoriesdao->retrieve_by_id($cat);
                $product = new Product($name, $stock, $price, $img_path, $category);

                $this->productsdao->create($product);

                header("Location: ../products?success=1");
            }
        }
    }

    /**
     * Función auxiliar para validar el input de imágenes
     */
    private function validate_img()
    {
        $img_dir = 'images/';
        $total_dir = ROOT . 'images/';

        if(!file_exists($total_dir))
            mkdir($total_dir);

        if($_FILES)
        {
            if((isset($_FILES['file'])) && ($_FILES['file']['name'] != ''))
            {
                $file = $total_dir . basename($_FILES['file']['name']);			

                $extension = pathinfo($file, PATHINFO_EXTENSION);

                $img_info = getimagesize($_FILES['file']['tmp_name']);
                
                if($img_info !== false)
                {
                    if(!file_exists($file))
                    {
                        if($_FILES['file']['size'] < 5000000) // menos de 5mb
                        {
                            if (move_uploaded_file($_FILES["file"]["tmp_name"], $file))
                            {
                                //echo 'el archivo '.basename($_FILES["file"]["name"]).' fue subido correctamente.';
                                return $img_dir . basename($_FILES["file"]["name"]);
                            } else {
                                // no se pudo subir el archivo
                                return null;
                            }
                        } else {
                            // el archivo es muy grande (mayor a 5mb)
                            return null;
                        }
                    } else {
                        // ya existe el archivo
                        return null;
                    }
                } else {
                    // no era una imagen
                    return null;
                }
            }		
        }
    }
}

?>